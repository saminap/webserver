#ifndef SERVER_HH
#define SERVER_HH

#define BUFFER_SIZE 512
#define EPOLL_QUEUE_LEN 64

#include "request.hh"
#include "response.hh"

#include <algorithm>
#include <arpa/inet.h>
#include <iostream>
#include <sys/socket.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <string>
#include <cstdio>
#include <cstring>
#include <stdint.h>
#include <sys/epoll.h>
#include <bitset>
#include <fcntl.h>

class Server{

public:
    Server(uint16_t port);
    ~Server();
    void start();

private:
    int port;
    int server_fd;
    int epoll_fd;
    struct epoll_event *events;
    struct sockaddr_in server;

    void setNonBlocking(int fd) const;
    void createEpollEvent(int fd, uint32_t events=EPOLLIN | EPOLLET);
    void newConnection();
    void handleRequest(struct epoll_event *event);
    void closeConnection(struct epoll_event *event);
};

#endif
