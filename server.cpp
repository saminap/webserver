
#include "server.hh"

Server::Server(uint16_t port): port(port){

    bzero((char *) &server, sizeof(server));

    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons(port);

    server_fd = socket(AF_INET, SOCK_STREAM, 0);

    if(server_fd < 0){
        std::cout << "Error creating socket" << std::endl;
        exit(1);
        // TODO: throw an exception maybe?
    }

    if(bind(server_fd, (struct sockaddr *)&server, sizeof(server)) < 0){
        std::cout << "Could not bind server socket" << std::endl;
        exit(1);
        // TODO: Throw an exception maybe?
    }

    epoll_fd = epoll_create1(0);
    events = (struct epoll_event*)calloc (EPOLL_QUEUE_LEN, sizeof(struct epoll_event));

    setNonBlocking(server_fd);
    listen(server_fd, 5);
    createEpollEvent(server_fd, EPOLLIN | EPOLLPRI | EPOLLERR | EPOLLHUP);

}

Server::~Server(){
    close(server_fd);
    free(events);
}

void Server::start(){

    while(true){
        std::cout << "Listening for connections" << std::endl;

        int numberOfDescriptors = epoll_wait (epoll_fd, events, EPOLL_QUEUE_LEN, -1);

        for(int i=0; i<numberOfDescriptors; ++i){
            if(events[i].data.fd == server_fd){
                newConnection();
            }
            else{
                if(events[i].events & EPOLLIN){
                    handleRequest(&events[i]);
                }
            }
        }
    }
}

void Server::setNonBlocking(int fd) const{
    int flags;
    flags = fcntl (fd, F_GETFL, 0);
    flags |= O_NONBLOCK;
    fcntl (fd, F_SETFL, flags);
}

void Server::createEpollEvent(int fd, uint32_t events){
    struct epoll_event event;
    event.events = events;
    event.data.fd = fd;

    epoll_ctl(epoll_fd, EPOLL_CTL_ADD, fd, &event);
}

void Server::newConnection(){
    struct sockaddr_in client;
    int client_fd;
    socklen_t addrlen = (socklen_t)sizeof(client);
    char ip[INET_ADDRSTRLEN];

    client_fd = accept(server_fd, (struct sockaddr *) &client, &addrlen);
    if(client_fd == -1){
        std::cout << "Accept failed" << std::endl;
        // TODO: throw an exception?
    }

    inet_ntop(AF_INET, &client.sin_addr, ip, INET_ADDRSTRLEN);

    std::cout << "connection from ip: " << ip << std::endl;

    setNonBlocking(client_fd);
    createEpollEvent(client_fd);
}

void Server::handleRequest(struct epoll_event *event){

    char buffer[BUFFER_SIZE+1];
    Response response;
    response.addJsonData("success", "ok");

    bzero(buffer, BUFFER_SIZE+1);

    if(read(event->data.fd, buffer, BUFFER_SIZE) > 0){
        Request request(buffer);
        std::cout << request.getMethod() << std::endl;
        response.send(event->data.fd);
    }
    else {
        //closeConnection(event);
    }
}

void Server::closeConnection(struct epoll_event *event){
    close(event->data.fd);
}
