
#include "response.hh"

Response::Response(): dataframe("HTTP/1.1 200 OK\r\nContent-Type: application/json; charset=UTF-8\r\nConnection: keep-alive\r\n"),
payload("")
{}

void Response::addJsonData(const std::string &key, const std::string &value){
    payload += "{\r\n\"" + key + "\": \"" + value + "\"\r\n}\r\n";
}

void Response::send(int descriptor){

    dataframe += "Content-Length: " + std::to_string(payload.length()) + "\r\n\r\n";

    dataframe += payload;

    write(descriptor, dataframe.c_str(), dataframe.size());
}
