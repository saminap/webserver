
#ifndef RESPONSE_HH
#define RESPONSE_HH

#include <unistd.h>
#include <string>
#include <vector>
#include <iostream>

class Response{

    public:
        Response();
        void addJsonData(const std::string &key, const std::string &value);
        void send(int descriptor);

    private:
        std::string dataframe;
        std::string payload;

};
#endif
