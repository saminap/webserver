
#ifndef REQUEST_HH
#define REQUEST_HH

#include <string>
#include <vector>
#include <iostream>

class Request{

    public:

        const static std::vector<std::string> ALLOWED_METHODS;

        Request(const char *buffer);
        const std::string &getMethod() const;

    private:
        std::string method;

};


#endif
