
#include "request.hh"

const std::vector<std::string> Request::ALLOWED_METHODS{"GET", "POST", "PUT", "DELETE"};


Request::Request(const char *buffer){
    std::string request(buffer);
    this->method = request.substr(0, request.find(" "));
}

const std::string &Request::getMethod() const{
    return method;
}
