
#include "server.hh"

#include <iostream>

int main(){

    Server server(8888);
    server.start();

    return 0;
}
